import React, { useState } from 'react';
import logo from './logo.svg';
import { BrowserRouter as Router,
  Route,
  Redirect,
Switch } from "react-router-dom";
import Login from "./components/Login"
import UserProfile from "./components/UserProfile"
import Auth from "./Auth"
  import './App.css';

function App() {

  const [loginStatus, setloginStatus] = useState(false);

  Auth.isAuthenticated().then(response => setloginStatus(response))

  return (
    <div className="App">
      <Router>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Switch>
          <Route exact path="/" render={() => loginStatus ? <Redirect to='/user-profile' /> : <Login />} />
          <Route path="/user-profile" render={ () => loginStatus ? <UserProfile userData={Auth.getData()} /> : <Redirect to="/" /> } />
         
          {/* <Route path="/loginCallback" render={() => Auth.setToken()} /> */}
          <Route path="/loginCallback" render={() => Auth.processAuthCode()} />
          <Route path="/logout" render={() => Auth.logout()} />
        </Switch>
      </header>
      </Router>
    </div>
  );
}

export default App;
