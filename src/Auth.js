import config from "./config/config"
import qs from 'qs'
import axios from 'axios';
const jwt = require('jsonwebtoken')
const jwkToPem = require('jwk-to-pem')


const Auth = {
    verifyToken : async (token) => {

        const publicKeys = await fetch(`https://cognito-idp.${config.region}.amazonaws.com/${config.userPoolId}/.well-known/jwks.json`).then( data => data.json())
        let decoded = await jwt.decode(token, { complete : true})
        if(!decoded){

            return false
        }

        let jwk = ""
        for(let i = 0;i<publicKeys.keys.length; i++){

            if(publicKeys.keys[i].kid === decoded.header.kid){
                jwk = publicKeys.keys[i]
                break
            }
        }

        const pem = jwkToPem(jwk);
        return jwt.verify(token, pem, { algorithms: ['RS256'] }, function(err, decodedToken) {


            if(err){
                return false
            }
            return true
            
        });

    },
    getData : () => jwt.decode(localStorage.getItem('auth-token')),
    isAuthenticated : async function(){ return localStorage.getItem("auth-token") ? await this.verifyToken(localStorage.getItem("auth-token")) : false } ,
    setToken : () => {
        let queryString = qs.parse(window.location.hash.split('#')[1])
        localStorage.setItem('auth-token',queryString.id_token)
        window.location = "/user-profile"
    },
    processAuthCode : () => {
        let queryString = qs.parse(window.location.search.split('?')[1])
        
        let axiosRequest = {
            url : "https://react-login-poc.auth.us-east-2.amazoncognito.com/oauth2/token",
            data : `grant_type=authorization_code&client_id=${config.appClientId}&code=${queryString.code}&redirect_uri=${config.redirectUri}`,
            headers : {
                headers : {
                    Authorization : "Basic NjJmNGxocWpxNW8ybjA0NjU4azNrb2ttczoxNHU3OGllY2NycGwxczloMGg1bzBvaW91Y25wdTE0cXNnOGRrOTViMjBwZG9zdnNrZG4=",
                    "Content-Type" : "application/x-www-form-urlencoded"
                }
            }
        }
        axios.post(axiosRequest.url,axiosRequest.data,axiosRequest.headers).then ( response => {
            localStorage.setItem('auth-token',response.data.id_token)
            window.location = "/user-profile"
        })
    },
    logout : () => {
        localStorage.removeItem('auth-token')
        window.location = "/"
    }
}


export default Auth 