import React from 'react'
import {Link} from 'react-router-dom'
const UserProfile = (props) => {
    console.log(props.userData)
    return (
        <div>
            <h2>
                 Hello {props.userData['cognito:username']} 
            </h2>
            
            <h4>User Details:</h4>
            <p>
            Subject : {props.userData.sub} <br />
            email : {props.userData.email} <br />
            </p>
            <Link to="/logout" className="App-link">
                Logout
            </Link>
        </div>
    )
}

export default UserProfile