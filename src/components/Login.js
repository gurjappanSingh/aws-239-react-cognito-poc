import React from 'react'
import '../App.css';
const Login = () => {
    return (
        <div>
            <p>
                Hello Guest! Please login to continue.
            </p>
            <a
            className="App-link"
            href="https://react-login-poc.auth.us-east-2.amazoncognito.com/login?client_id=62f4lhqjq5o2n04658k3kokms&response_type=code&scope=aws.cognito.signin.user.admin+email+openid+phone+profile&redirect_uri=https://angry-kalam-e9c6b0.netlify.com/loginCallback"
            rel="noopener noreferrer"
            >
            Log In
            </a>
        </div>
    )
}

export default Login